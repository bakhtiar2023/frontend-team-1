import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

function MyVerticallyCenteredModal(props) {
  // eslint-disable-next-line no-unused-vars
  return (
    <Modal
      show={props.show}
      onHide={props.onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title
          id="contained-modal-title-vcenter"
          className="fw-600 text-dark"
        >
          Edit Profile Foto
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <label htmlFor="formFile" className="form-label">
          Choose your profile foto
        </label>
        <input
          type="file"
          className="form-control"
          id="formFile"
          onChange={props.handlechange}
        />
        <button
          className="btn btn-primary mt-4"
          onClick={props.handleclick}
        >
          Upload
        </button>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default MyVerticallyCenteredModal;
