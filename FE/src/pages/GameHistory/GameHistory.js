/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import jwt_decode from "jwt-decode";
import "bootstrap/dist/css/bootstrap.css";
import "../App.css";

function GameHistory() {
  const accessToken = localStorage.getItem("accessToken");
  const authorization = accessToken.split(" ")[1];
  const token = jwt_decode(authorization);
  const [errMsg, setErrMsg] = useState("");
  const [loading, setLoading] = useState(true);
  const [loading2, setLoading2] = useState(true);
  const [isNull, setIsNull] = useState(false);
  const [value, setValue] = useState(false);
  const [histories, setHistories] = useState([
    {
      status: null,
      roomId: null,
      createdAt: null,
      gameRoom: null,
      date: null,
      time: null,
    },
  ]);

  const getAllHistory = async () => {
    try {
      const getAllHistoryGame = await axios
        .get(
          `https://backend-team-1-five.vercel.app/history/${token.id.toString()}`,
          {
            headers: { Authorization: `${accessToken}` },
          }
        )
        .then((response) => {
          return response.data.message;
        });
      const restructuredData = [];
      getAllHistoryGame.map((histories) => {
        return restructuredData.push({
          status: histories.status,
          roomId: histories.roomId,
          createdAt: histories.createdAt,
          gameRoom: histories.gameRoom,
          date: histories.createdAt.split("T")[0],
          time: histories.createdAt.split("T")[1].split(".")[0],
        });
      });
      setLoading(false);
      if (restructuredData.length < 1) {
        setIsNull(true);
        setHistories(restructuredData);
      } else {
        setIsNull(false);
        setHistories(restructuredData);
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getAllHistory();
  });

  return (
    <div className="tableContainer container ">
      <h3 className="title fs-3 text-start ms-2 text-center">Game History</h3>
      <table className="table table-fixed">
        <thead>
          <tr>
            <th className="col-xl-3 col-xs-3 text-center">Username</th>
            <th className="col-xl-2 col-xs-3 text-center">Status</th>
            <th className="col-xl-3 col-xs-3 text-center">Room Name</th>
            <th className="col-xl-2 col-xs-3 text-center">Date</th>
            <th className="col-xl-2 col-xs-3 text-center">Time</th>
          </tr>
        </thead>
        <tbody>
          {histories.map((getAllHistoryGame, i) => {
            return (
              <tr key={i}>
                <td className="col-xl-3 col-xs-3 text-center">
                  {token.username}
                </td>
                <td className="col-xl-2 col-xs-3 text-center">
                  {getAllHistoryGame.status === null
                    ? "....."
                    : getAllHistoryGame.status}
                </td>
                <td className="col-xl-3 col-xs-3 text-center">
                  {getAllHistoryGame.gameRoom === null
                    ? "....."
                    : getAllHistoryGame.gameRoom.roomName}
                </td>
                <td className="col-xl-2 col-xs-2 text-center">
                  {getAllHistoryGame.date === null
                    ? "....."
                    : getAllHistoryGame.date}
                </td>
                <td className="col-xl-2 col-xs-2 text-center">
                  {getAllHistoryGame.time === null
                    ? "....."
                    : getAllHistoryGame.time}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default GameHistory;
